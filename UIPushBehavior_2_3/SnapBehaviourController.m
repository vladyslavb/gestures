//
//  SnapBehaviourController.m
//  UIPushBehavior_2_3
//
//  Created by Vladyslav Bedro on 6/13/18.
//  Copyright © 2018 Vladyslav Bedro. All rights reserved.
//

#import "SnapBehaviourController.h"

@interface SnapBehaviourController ()

// properties
@property (strong, nonatomic) IBOutlet UIView* squareView;
@property (strong, nonatomic) UIDynamicAnimator* animator;
@property (strong, nonatomic) UISnapBehavior* snapBehavior;

- (IBAction)onTapGesture:(UITapGestureRecognizer *)sender;

@end

@implementation SnapBehaviourController


#pragma mark - Life cycle -

- (void) viewDidLoad
{
    [super viewDidLoad];
}

- (void) viewDidAppear: (BOOL) animated
{
    [super viewDidAppear: animated];
    
    [self createAnimatorAndBehaviours];
}


#pragma mark - Memory Managment -

- (void) didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
}


#pragma mark - Internal methods -

- (void) createAnimatorAndBehaviours
{
    self.animator = [[UIDynamicAnimator alloc] initWithReferenceView: self.view];
    
    // Collision
    UICollisionBehavior* collision = [[UICollisionBehavior alloc] initWithItems: @[self.squareView]];
    collision.translatesReferenceBoundsIntoBoundary = YES;
    [self.animator addBehavior: collision];
    
    // Snap
    self.snapBehavior = [[UISnapBehavior alloc] initWithItem: self.squareView
                                                                              snapToPoint: self.squareView.center];
    self.snapBehavior.damping = 0.5f;
    [self.animator addBehavior: self.snapBehavior];
}


#pragma mark - Tap actions -

- (IBAction) onTapGesture: (UITapGestureRecognizer*) sender
{
    CGPoint tapPoint = [sender locationInView: self.view];
    if (self.snapBehavior != nil)
    {
        [self.animator removeBehavior: self.snapBehavior];
    }
    
    self.snapBehavior = [[UISnapBehavior alloc] initWithItem: self.squareView
                                                                              snapToPoint: tapPoint];
    self.snapBehavior.damping = 0.5f;
    [self.animator addBehavior: self.snapBehavior];
}
@end















