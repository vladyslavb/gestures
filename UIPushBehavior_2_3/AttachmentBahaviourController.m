//
//  AttachmentBahaviourController.m
//  UIPushBehavior_2_3
//
//  Created by Vladyslav Bedro on 6/13/18.
//  Copyright © 2018 Vladyslav Bedro. All rights reserved.
//

#import "AttachmentBahaviourController.h"

@interface AttachmentBahaviourController ()

// Properties
@property (strong, nonatomic) IBOutlet UIView* squareView;
@property (strong, nonatomic) IBOutlet UIView* squareViewAnchorView;
@property (strong, nonatomic) IBOutlet UIView* anchorView;
@property (strong, nonatomic) UIDynamicAnimator* animator;
@property (strong, nonatomic) UIAttachmentBehavior* attachmentBehavior;

// Actions
- (IBAction) onPanGesture: (id) sender;

@end

@implementation AttachmentBahaviourController


#pragma mark - Life cycle -

- (void) viewDidLoad
{
    [super viewDidLoad];
}

- (void) viewDidAppear: (BOOL) animated
{
    [super viewDidAppear: animated];
    
    [self createAnimatorAndBehaviours];
}

#pragma mark - Memory methods -

- (void) didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
}


#pragma mark - Internal methods -

- (void) createAnimatorAndBehaviours
{
    self.animator = [[UIDynamicAnimator alloc] initWithReferenceView: self.view];
    
    // Create recognizition of collisions
    UICollisionBehavior* collision = [[UICollisionBehavior alloc] initWithItems: @[self.squareView]];
    collision.translatesReferenceBoundsIntoBoundary = YES;
    
    UIOffset offset = UIOffsetMake(self.squareViewAnchorView.center.x, self.squareViewAnchorView.center.y);
    
    self.attachmentBehavior = [[UIAttachmentBehavior alloc] initWithItem: self.squareView
                                                                                           offsetFromCenter: offset
                                                                                           attachedToAnchor: self.anchorView.center];
    
    [self.animator addBehavior: collision];
    [self.animator addBehavior: self.attachmentBehavior];
}


#pragma mark - Tap actions -

- (IBAction) onPanGesture: (id) paramPan
{
    CGPoint tapPoint = [paramPan locationInView: self.view];
    [self.attachmentBehavior setAnchorPoint: tapPoint];
    self.anchorView.center = tapPoint;
}

@end
















