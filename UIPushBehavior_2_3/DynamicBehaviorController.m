//
//  DynamicBehaviorController.m
//  UIPushBehavior_2_3
//
//  Created by Vladyslav Bedro on 6/13/18.
//  Copyright © 2018 Vladyslav Bedro. All rights reserved.
//

#import "DynamicBehaviorController.h"

@interface DynamicBehaviorController ()

// propeties
@property (strong, nonatomic) UIDynamicAnimator* animator;
@property (strong, nonatomic) IBOutlet UIView* topView;
@property (strong, nonatomic) IBOutlet UIView* bottomView;

@end

@implementation DynamicBehaviorController


#pragma mark - Life cycle -

- (void) viewDidLoad
{
    [super viewDidLoad];
}

- (void) viewDidAppear: (BOOL) animated
{
    [super viewDidAppear: animated];
    
    [self createAnimatorAndBehaviours];
}


#pragma mark - Memory managment -

- (void) didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
}


#pragma mark - Internal methods -

- (void) createAnimatorAndBehaviours
{
    self.animator = [[UIDynamicAnimator alloc] initWithReferenceView:self.view];
    
    UIGravityBehavior* gravity = [[UIGravityBehavior alloc] initWithItems: @[self.topView, self.bottomView]];
    [self.animator addBehavior: gravity];
    
    UICollisionBehavior* collision = [[UICollisionBehavior alloc] initWithItems: @[self.topView, self.bottomView]];
    collision.translatesReferenceBoundsIntoBoundary = YES;
    [self.animator addBehavior: collision];
    
    UIDynamicItemBehavior* moreElasticElement = [[UIDynamicItemBehavior alloc] initWithItems: @[self.bottomView]];
    moreElasticElement.elasticity = 1.0f;
    
    UIDynamicItemBehavior* lessElasticElement = [[UIDynamicItemBehavior alloc] initWithItems: @[self.topView]];
    lessElasticElement.elasticity = 0.5f;
    
    [self.animator addBehavior: moreElasticElement];
    [self.animator addBehavior: lessElasticElement];
}

@end







