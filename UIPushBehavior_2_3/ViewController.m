//
//  ViewController.m
//  UIPushBehavior_2_3
//
//  Created by Vladyslav Bedro on 6/12/18.
//  Copyright © 2018 Vladyslav Bedro. All rights reserved.
//

#import "ViewController.h"

@interface ViewController ()

// Properties
@property (strong, nonatomic) IBOutlet UIView* squareView;
@property (strong, nonatomic) UIDynamicAnimator* animator;
@property (strong, nonatomic) UIPushBehavior* pushBehaviour;

// Actions
- (IBAction) onTapGesture: (UITapGestureRecognizer*) sender;

@end

@implementation ViewController


#pragma mark - Life cycle -

- (void) viewDidLoad
{
    [super viewDidLoad];
}

- (void) viewDidAppear: (BOOL) animated
{
    [super viewDidAppear: animated];
    
    [self createAnimatorAndBehaviours];
}


#pragma mark - Memory methods -

- (void) didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
}


#pragma mark - Internal methods -

- (void) createAnimatorAndBehaviours
{
    self.animator = [[UIDynamicAnimator alloc] initWithReferenceView:self.view];
    
    /* Создаем обнаружение столкновений */
    UICollisionBehavior* collision = [[UICollisionBehavior alloc] initWithItems: @[self.squareView]];
    collision.translatesReferenceBoundsIntoBoundary = YES;
    
    self.pushBehaviour = [[UIPushBehavior alloc] initWithItems: @[self.squareView]
                                                                                             mode: UIPushBehaviorModeContinuous];
    
    [self.animator addBehavior: collision];
    [self.animator addBehavior: self.pushBehaviour];
}


#pragma mark - Tap actions -

- (IBAction) onTapGesture: (UITapGestureRecognizer*) paramTap
{
    CGPoint tapPoint = [paramTap locationInView: self.view];
    CGPoint squareViewCenterPoint = self.squareView.center;
    
    // Get angle between central point of squareView and point of Tap
    // Formula for determining angle between two points: arc tangent 2((p1.x - p2.x), (p1.y - p2.y))
    CGFloat deltaX = tapPoint.x - squareViewCenterPoint.x;
    CGFloat deltaY = tapPoint.y  - squareViewCenterPoint.y;
    CGFloat angle  = atan2(deltaY, deltaX);
    [self.pushBehaviour setAngle: angle];
    
    /*
     * Get power of push
     * Formula for determining distance:
     * Sqrt from ((p1.x - p2.x)^2 + (p1.y - p2.y)^2)
     */
    CGFloat distanceBetweenPoints = sqrt(pow(tapPoint.x - squareViewCenterPoint.x, 2.0) +
                                         pow(tapPoint.y - squareViewCenterPoint.y, 2.0));
    [self.pushBehaviour setMagnitude: distanceBetweenPoints / 200.0f];
}

@end





















